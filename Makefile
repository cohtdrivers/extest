# If it exists includes Makefile.specific. In this Makefile, you should put
# specific Makefile code that you want to run before this. For example,
# build a particular environment.
-include Makefile.specific

# define REPO_PARENT to get build environment
REPO_PARENT?=..
-include $(REPO_PARENT)/parent_common.mk

-include ../version.mk

CFLAGS = -Wall -ggdb -fPIC -Werror -I./
CFLAGS += -D__GIT_VER__="\"$(GIT_VER)\"" -D__GIT_USR__="\"$(GIT_USR)\""

LIB = libextest.a

LIB_OBJS = extest.o
LIB_SRCS = extest.c

modules all: $(LIB)

clean:
	$(RM) *.o *.so *.a

cleanall:
	$(RM) *.{$(ALL_CPUS_COMMAS)}.o *.{$(ALL_CPUS_COMMAS)}.so *.{$(ALL_CPUS_COMMAS)}.a

%: %.c $(LIB)
	$(CC) $(CFLAGS) $*.c $(LDFLAGS) -o $@

libextest.a: $(LIB_OBJS)
	ar r $@ $^

.depend: Makefile $(wildcard *.c *.h)
	$(CC) $(CFLAGS) -M $(LIBOBJS.o=.c) -o $@

# use default instalation rule for libs
install: install_libs_global

# use default instalation rule for libs
install: install_prog_global
